#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

class MssCreditos():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ui/Archivo_Ventana.ui")
        self.Messenge = builder.get_object("Ventana_Creditos")
        self.Messenge.set_title("Creditos")
        cerrar = builder.get_object("btnCerrar")
        cerrar.connect("clicked", self.on_btn_cerrar_clicked)

    def on_btn_cerrar_clicked(self, btn=None):
            self.Messenge.destroy()