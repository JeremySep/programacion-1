#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas 
import numpy
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Dlg_credits import Dlgcredits



class MainWindow():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/winwin.ui")
        self.window = self.builder.get_object("win_ven")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("U2 Proyecto1")
        self.window.resize(600, 400)

        boton_credits = self.builder.get_object("btncredits")
        boton_credits.connect("clicked", self.boton_credits_clicked)

        lista = ["¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?",
                "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?",
                "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?",
                "¿Cuántas personas han sido afectadas de alguna manera por el SARS COVID 2 en el mundo?",
                "¿En el tiempo en que se recolectaron los datos, sepuede decir que existe inmunidad de grupo en el mundo?"]

        self.comboboxtext = self.builder.get_object("comboboxtext")
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
        self.comboboxtext.set_entry_text_column(0)
        for index in lista:
            self.comboboxtext.append_text(index)
        
        self.liststore = self.builder.get_object("tree_view")
        self.liststore.connect("cursor-changed", self.liststore_changed)

        self.label_label= self.builder.get_object("labeltext")

        self.label_respuesta= self.builder.get_object("label_respuesta")

        self.window.show_all()
        self.get_dataframe()
    
    def get_dataframe(self):
    
        dataframe = "./suministro_alimentos_kcal.csv"
        data = pandas.read_csv(dataframe)
        data.drop(["Animal Products", "Animal fats","Undernourished",
                    "Aquatic Products, Other", "Cereals - Excluding Beer",
                    "Eggs", "Fish, Seafood", "Fruits - Excluding Wine",
                    "Meat", "Milk - Excluding Butter", "Miscellaneous",
                    "Offals", "Oilcrops", "Pulses", "Spices", "Starchy Roots",
                    "Stimulants", "Sugar Crops", "Sugar & Sweeteners",
                    "Treenuts", "Vegetal Products", "Vegetable Oils", "Vegetables",
                    "Unit (all except Population)"],
                    axis=1, inplace=True)
        data=data.dropna()
        self.data_limpio=data.drop(0.0000, axis=0)
    

    def comboboxtext_changed(self, cmb=None):
        self.valor_combobox_text = self.comboboxtext.get_active_text() 

        if self.valor_combobox_text=="¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?":
            self.data_frame=self.combo1()
        elif self.valor_combobox_text=="¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?":
            self.data_frame=self.combo2()
        elif self.valor_combobox_text=="¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?":
            self.data_frame=self.combo3()
        elif self.valor_combobox_text=="¿Cuántas personas han sido afectadas de alguna manera por el SARS COVID 2 en el mundo?":
            self.data_frame=self.combo4()
        elif self.valor_combobox_text=="¿En el tiempo en que se recolectaron los datos, sepuede decir que existe inmunidad de grupo en el mundo?":
            self.data_frame=self.combo5()

        self.intro_data()

    def boton_credits_clicked(self, btn=None):
        dlg = Dlgcredits()
        dlg.dialogo.run()
    
   
    def combo1(self):
        data_obesidad=self.data_limpio.sort_values(by=["Obesity"])
        respuestaP=["0"for i in range(5)]
        respuesta=["0"for i in range(5)]

        popular=data_obesidad["Population"]
        obeso=data_obesidad["Obesity"]
        pais=data_obesidad["Country"]
        data_frame=data_obesidad[["Country","Obesity","Population"]]
        resultado=["0"for i in range(len(pais))]
        for e in range(5):
            for i in range(len(pais)):
                listaO=list(obeso)
                lista=list(pais)
                listaP=list(popular)
                resultado[i]=(listaO[i]/100)*listaP[i]
            respuestaP[e]=lista[i-e]
            respuesta[e]=resultado[i-e]
        self.respuestaF=("Los países con más peronas obesas son:",respuestaP,respuesta)
        self.label_resp()
        return data_frame
                
    def combo2(self):
        data_obesidad=self.data_limpio.sort_values(by=["Obesity"])
        popular=data_obesidad["Population"]
        obeso=data_obesidad["Obesity"]
        pais=data_obesidad["Country"]
        muerte=data_obesidad["Deaths"]
        data_frame=data_obesidad[["Country", "Population", "Obesity", "Deaths"]]
        resultado=["0"for i in range(len(pais))]
        dicc={}
        respuesta=["0"for i in range(5)]
        respuestaP=["0"for i in range(5)]
        for e in range(5):
            for i in range(len(pais)):
                listaO=list(obeso)
                lista=list(pais)
                listaP=list(popular)
                listaM=list(muerte)
                resultado[i]=(listaO[i]/100)*listaP[i]
                resultado[i]=resultado[i]+((listaM[i]/100)*listaP[i])
                dicc[resultado[i]]=lista[i]
                resp=list(dicc.keys())
                resp.sort()
            respuesta[e]=resp[i-e]
            respuestaP[e]=dicc[respuesta[e]]
        self.respuestaF=("Los países con mas muerteis y obesos son:",respuestaP,respuesta)
        self.label_resp()
        return data_frame
    
    def combo3(self):
        data_bebida=self.data_limpio.sort_values(by=["Alcoholic Beverages"])
        pais=data_bebida["Country"]
        bebida=data_bebida["Alcoholic Beverages"]
        casos=data_bebida["Confirmed"]
        pobla=data_bebida["Population"]
        data_frame=self.data_limpio[["Country", "Alcoholic Beverages", "Population", "Confirmed"]]
        dicc1={}
        dicc2={}
        listas1=["0"for i in range(10)]
        listas2=["0"for i in range(10)]
        listasC=["0"for i in range(10)]
        listasA=["0"for i in range(10)]

        for e in range(10):
            for i in range(len(pais)):
                lista=list(pais)
                listaA=list(bebida)
                listaC=list(casos)
                listaP=list(pobla)
                listaA[i]=listaA[i]/100
                listaA[i]=listaA[i]*listaP[i]
                listaC[i]=listaC[i]*listaP[i]
                listaC[i]=listaC[i]/100
                dicc1[listaA[i]]=lista[i]
                dicc2[listaC[i]]=lista[i]
                resp1=list(dicc1.keys())
                resp1.sort()
                resp2=list(dicc2.keys())
                resp2.sort()
                listas2[e]=resp2[i-e]
                listasC[e]=dicc2[listas2[e]]
            for i in range(len(resp1)):
                listas1[e]=resp1[i-e]
                listasA[e]=dicc1[listas1[e]]
        self.respuestaF=("Los alcoholicos son:",listasA,listas1," y los con países con más casos confirmados son:",listasC,listas2)
        self.label_resp()     
        return data_frame

    def combo4(self):
        casos=self.data_limpio["Confirmed"]
        muerte=self.data_limpio["Deaths"]
        pobla=self.data_limpio["Population"]
        pais=self.data_limpio["Country"]
        data_frame=self.data_limpio[["Country", "Population", "Deaths", "Confirmed"]]
        personasA=0
        for i in range(len(pais)):
            listaM=list(muerte)
            listaP=list(pobla)
            listaC=list(casos)
            respuesta= personasA+((listaC[i]/100)*listaP[i])+((listaM[i]/100)*listaP[i])
        self.respuestaF=respuesta
        self.label_resp()
        return data_frame
    
    def combo5(self):
        casos=self.data_limpio["Confirmed"]
        pobla=self.data_limpio["Population"]
        data_frame=self.data_limpio[["Country", "Confirmed", "Population"]]
        personasC=0
        personas=0
        for i in range(len(pobla)):
            listaC=list(casos)
            listaP=list(pobla)
            personasC=personasC+((listaC[i]/100)*listaP[i])
            personas=personas+listaP[i]
        respuesta=personas/(personasC*100)
        self.respuestaF=respuesta
        self.label_resp()
        return data_frame
    
    def intro_data(self):
        self.model = Gtk.ListStore(*((len(self.data_frame.columns)) * [str]))
        self.liststore.set_model(model=self.model)
        self.data_frame=self.data_frame.sort_index()
        cell = Gtk.CellRendererText()
        
        for i in range(len(self.data_frame.columns)):
            column = Gtk.TreeViewColumn(self.data_frame.columns[i],
                                        cell,
                                        text=i)
            self.liststore.append_column(column)
        for value in self.data_frame.values:
            row = [str(i) for i in value]
            self.model.append(row) 

    def label_resp(self):
        try:
            res = str(self.respuestaF)
            res = ''.join(["La respuesta es: ",
                            res])
        except:
            error=1                            
        self.label_respuesta.set_text(res)


    def liststore_changed(self, tree=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        nombre = model.get_value(it, 0)
        nombre = ''.join(["Nombre: ",
                          nombre])
        self.label_label.set_text(nombre)

  


    

    
if  __name__ == "__main__":
    MainWindow()
    Gtk.main()