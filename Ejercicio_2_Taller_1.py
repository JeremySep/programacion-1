#Se inician sumatorias en 0 y se asiganan las constantes de descuentos
sumadescuento=0
sumaprecio=0
oferta1= 0.15
oferta2= 0.08
#Se pide la cantidad de productos a procesar
fin= int(input("Ingrese la cantidad de productos va a comprar (minimo 3) \n"))
#Segun la cantidad de productos se repetira el ciclo
for i in range(0,fin):
#Se pide el valor de vestido
    vestido= int(input("Ingrese el precio del producto:"))
#Se reinicia la variable en cada ciclo
    descuento=0
#se utilizan condicionales para saber que oferta usar segun el valor del vestido
    if vestido > 10000:
        descuento= vestido*oferta1
        precio= vestido-descuento
    else:
        descuento= vestido*oferta2
        precio= vestido-descuento
#Se realizan las sumatorias del precio y del descuento
    sumadescuento= sumadescuento+descuento
    sumaprecio= sumaprecio+precio
#Se imprimen la cantidad de vestidos y las sumatorias de descuentos y precios
print ("En la compra de",fin," productos usted ha ahorrado: ",sumadescuento,"pesos pagando en total: ",sumaprecio,"pesos")
    