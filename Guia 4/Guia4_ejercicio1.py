def crear_menu():
    menu= {"2HLD":"Estructura cristalina de la F1-ATPasa mitocondial de la levadura", 
    "6RD9": "Estructura CryoEM de Polytomella F-ATP sintasa, estado rotatorio primario 1, mapa compuesto"}
    return menu
   
def imprimir_menu(menu):
    print(menu)
    
def sumar_menu(menu,seleccion):
    menu[seleccion]= input("Ingrese datos")
    return menu
    
def cambiar_menu(menu,seleccion):
    menu[seleccion]= input("Ingrese cambios")
    return menu
    
def eliminar_menu(menu,seleccion):
    del menu[seleccion]
    return menu

def consulta_menu(menu,seleccion):
    print(menu[seleccion])
    
menu=crear_menu()

eleccion=str

salir=0

while True:
    imprimir_menu(menu)
    
    print("Imprimir, Eliminar, Añadir, Cambiar, Consulta o Salir")
    
    eleccion= input("Ingrese la acción que desea ejecutar\n")
    if eleccion.upper()=="SALIR":
        break
    print(" ".join(menu.keys()))
    seleccion= input("Palabra que quiere realizar la accion\n")
    
    if eleccion.upper()=="ELIMINAR":
        eliminar_menu(menu,seleccion)
    elif eleccion.upper()=="IMPRIMIR":
        imprimir_menu(menu)
    elif eleccion.upper()=="CAMBIAR":
        cambiar_menu(menu,seleccion)
    elif eleccion.upper()=="AÑADIR":
        sumar_menu(menu,seleccion)
    elif eleccion.upper()=="CONSULTA":
        consulta_menu(menu,seleccion)
   
