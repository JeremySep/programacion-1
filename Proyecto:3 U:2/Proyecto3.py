#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
import pandas
import pymol
from gi.repository import Gtk
from biopandas . pdb import PandasPdb
gi.require_version('Gtk', '3.0')
from mssCredits import MssCreditos
from Filechooser import File_chooser

class wnPrincipal():
    def __init__(self):
        self.builder=Gtk.Builder()
        self.builder.add_from_file("./ui/Archivo_Ventana.ui")
        self.window=self.builder.get_object("Ventana_Principal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Principal")
        self.window.maximize()

        boton_credits=self.builder.get_object("btnCreditos")
        boton_credits.connect("clicked", self.boton_credits_clicked)

        boton_Abrir=self.builder.get_object("btnAbrir")
        boton_Guardar=self.builder.get_object("btnGuardar")
        boton_Abrir.connect("clicked", self.open_filechooser, "open")
        boton_Guardar.connect("clicked", self.open_filechooser, "save")

        self.liststore=self.builder.get_object("Tree_view")

        self.imagen=self.builder.get_object("imagen_ima")

        lista=["ATOM", "HETATM", "ANISOU", "OTHERS"]

        self.comboboxtext=self.builder.get_object("Comboboxtext")
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
        self.comboboxtext.set_entry_text_column(0)
        for index in lista:
            self.comboboxtext.append_text(index)        
        self.window.show_all()

    def boton_credits_clicked(self, btn=None):
        Mss=MssCreditos()
        Mss.Messenge.run()

    def open_filechooser(self, btn=None, opt=None):
        filechooser = File_chooser(option=opt)
        dialogo = filechooser.dialogo
        if opt == "open":
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name("pdb Files")
            filter_pdb.add_mime_type("application/x-aportisdoc")
            dialogo.add_filter(filter_pdb)

        response = dialogo.run()
        self.nombre=dialogo.get_filename()
        if response == Gtk.ResponseType.OK:
            self.update_tree(dialogo.get_filename())
        elif response == Gtk.ResponseType.ACCEPT:
            self.nombre_nuevo= dialogo.get_uri()
            self.nombre_nuevo= self.nombre_nuevo.split("/")
            self.nombre_nuevo= self.nombre_nuevo[-1]
            self.nombre_nuevo= self.nombre_nuevo.split(".")
            self.nombre_nuevo= self.nombre_nuevo[0]
            self.pdb_guardar_changed(dialogo.get_filename())

        dialogo.destroy()

    def update_tree(self,pdb):
        self.muta=pdb
        ppdb = PandasPdb()
        ppdb.read_pdb (pdb)
        self.ppdb=ppdb
        self.nombre=self.nombre.split("/")
        self.nombre=self.nombre[-1]
        self.nombrelimpio=self.nombre.split(".")
        self.nombrelimpio=self.nombrelimpio[0]
        self.gen_image()
    
    def pdb_guardar_changed(self,pdb):
        self.newfile=self.ppdb
        self.newfile.to_pdb(path = ('./'+self.nombre_nuevo+'.pdb') ,
        records =[self.variable] ,
        gz = False ,
        append_newline = True )
        
    def comboboxtext_changed(self, cmb=None):
        self.valor_combobox_text = self.comboboxtext.get_active_text()

        if self.valor_combobox_text=="ATOM":
            self.variable="ATOM"
        elif self.valor_combobox_text=="HETATM":
            self.variable="HETATM"
        elif self.valor_combobox_text=="ANISOU":
            self.variable="ANISOU"
        elif self.valor_combobox_text=="OTHERS":
            self.variable="OTHERS"
        
        self.cambia_pdb()

    def cambia_pdb(self):
        self.nuevo=self.ppdb.df[self.variable]
        self.nuevo=self.nuevo[:1000]
        self.intro_data()

    def gen_image(self):
        name=str(self.nombre)
        aux=name.split(".")
        pdb_name=aux[0]
        png_file="".join([self.muta])
        pdb_file=name
        pymol.cmd.load(self.muta, pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(pdb_name)
        pymol.cmd.png(png_file)
        pymol.cmd.ray()
        self.imagen.set_from_file(f"{png_file}.png")

    def intro_data(self):

        if self.liststore.get_columns():
                for column in self.liststore.get_columns():
                    self.liststore.remove_column(column)

        lista=list(self.nuevo)
        self.model = Gtk.ListStore(*((len(lista))*[str]))
        self.liststore.set_model(model=self.model)
        cell = Gtk.CellRendererText()

        for i in range(len(self.nuevo.columns)):
            column = Gtk.TreeViewColumn(self.nuevo.columns[i],
                                        cell,
                                        text=i)
            self.liststore.append_column(column)
        for value in self.nuevo.values:
            row = [str(i) for i in value]
            self.model.append(row) 
        
if __name__ == "__main__":
    wnPrincipal()
    Gtk.main()

    
