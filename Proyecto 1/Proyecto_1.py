import random

def crear_listas(largo):
    lista =[["| |" for i in range(largo)] for e in range(largo)]
    return lista

def impr_lista(lista):
    for i in range(20):
        texto= " ".join(lista[i])
        print(texto)

def crear_mapa(lista):
    for i in range(20):
        for e in range(20):
            lista[0][0]="|X|"
            lista[19][19]="|O|"
            camino= random.randrange(4)
            if camino==0 and i!=0:
                lista[i-1][e]="|O|"
            elif camino==1 and e!=0:
                lista[i][e-1]="|O|"
            elif camino==2 and e!=19:
                lista[i][e+1]="|O|"
            elif camino==3 and i!=19:
                lista[i+1][e]="|O|"
    return lista
    
def move_lista(lista):
    move= input("""Ingrese su movimiento:
los movimientos posibles son:
(A=izquierda; D=derecha; W=arriba; S=abajo)\n""")
    for i in range(20):
        for e in range(20):
            if move.upper()=="A":
                try:
                    if lista[i].index("|X|")!=0:
                        lista[i][e-1]="|X|"
                        lista[i][e]="|O|"
                        break
                except:
                    error=1
            elif move.upper()=="D":
                try:
                    if lista[i].index("|X|")!=len(lista):
                        lista[i][e+1]="|X|"
                        lista[i][e]="|O|"
                        break
                except: 
                    error=2
            elif move.upper()=="W":
                if i!=0:
                    lista[i-1][e]="|X|"
                    lista[i][e]="|O|"
                    break
            elif move.upper()=="S":
                if i!=19:
                    lista[i+1][e]="|X|"
                    lista[i][e]="|O|"
                    break
            elif move.upper()=="R" and lista[19][19]=="|X|":
                crear_mapa(lista)
    impr_lista(lista)
    return lista
    
largo=20
lista=crear_listas(largo)
crear_mapa(lista)
impr_lista(lista)
while exit!=0:
    try:
        move_lista(lista)
    except ValueError:
        print("El valor ingresado no es valido, utilice las letras(A;D;W;S) para moverse")
    except TypeError: 
        print("El tipo ingresado no es valido, utilice las letras (A;D;W;S) para moverse")