import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
Dataset= "./suministro_alimentos_kcal.csv"

def abrir_archi(Dataset):
    data= pd.read_csv(Dataset, delimiter=",", index_col=False)
    return data
    
def eliminar_columnas(data):
    data.drop(["Animal Products", "Animal fats","Undernourished",
                "Aquatic Products, Other", "Cereals - Excluding Beer",
                "Eggs", "Fish, Seafood", "Fruits - Excluding Wine",
                "Meat", "Milk - Excluding Butter", "Miscellaneous",
                "Offals", "Oilcrops", "Pulses", "Spices", "Starchy Roots",
                "Stimulants", "Sugar Crops", "Sugar & Sweeteners",
                "Treenuts", "Vegetal Products", "Vegetable Oils", "Vegetables",
                "Unit (all except Population)","Recovered", "Deaths","Country","Confirmed"],
                axis=1, inplace=True)
    print(data)
    return data

def pregun_1(data):
    data2=data.drop(["Active","Population"],axis=1, inplace=False)
    promedio = data2["Alcoholic Beverages"].mean()
    print("La tasa de ingesta de bebidas alcohólicas mundial es de:",promedio)
    return promedio

def pregun_2(data):
    data2=data.drop(["Active","Alcoholic Beverages"],axis=1, inplace=False)
    promedio2 = data2["Obesity"].mean()
    print("La tasa de obesidad mundial es de:",promedio2)
    return promedio2

def pregun_3(data):
    data2=data.drop(["Obesity","Alcoholic Beverages"],axis=1, inplace=False)
    promedio3 = data2["Active"].mean()
    print("La tasa de personas activas mundial en promedio es de:",promedio3)
    return promedio3

def pregun_4(data):
    promedio=pregun_1(data)
    promedio2=pregun_2(data)
    promedio3=pregun_3(data)
    data2=data.drop(["Obesity","Alcoholic Beverages"],axis=1, inplace=False)
    suma = data2["Population"].sum()
    print("Suma",suma)
    print(data2.columns)
    personA= suma*promedio3
    Mpeligro= (promedio*promedio2)/100
    personasP= personA*Mpeligro
    print(suma,personA,personasP)
    print("""La cantidad de personar que estan en peligro según las estadísticas obtenidas de la base de 
    datos es de:""", personasP)
    Graficodepoblación_pie(suma, personA,personasP)
        

def selec_preg(data):
    selec= input("""Seleccióne la pregunta que desea conocer la respuesta:
    1) cuál es la tasa mundial de ingesta de bebidas alcoholicas(promedio)?
    2) cuál es la tasa mundial de obesidad(Promedio)?
    3) cuál es el porcentaje promedio de contagios activos en el mundo(promedio)?
    4) Segun la información que entrega la Onu, ¿cuál es la tasa de personas con una 
    alta probabilidad de fallecer segun el factor de alimentación?\n""")

    if selec=="1":
        pregun_1(data)
    elif selec=="2":
        pregun_2(data)
    elif selec=="3":
        pregun_3(data)
    elif selec=="4":
        pregun_4(data)

def Graficodepoblación_pie(suma, personA, personasP):
    PersonaSR= personA-personasP
    resto=suma-personA
    x=[resto, PersonaSR, personasP]
    y=["Personas_sanas", "Casos_sin_riesgo", "Casos_de_riesgo"]
    plt.pie(x, labels=y)
    plt.show()
    plt.savefig("Graficodepoblación_pie.png")

def main():
    data=abrir_archi(Dataset)
    data=eliminar_columnas(data)
    selec_preg(data)

if __name__ == "__main__":
    main()