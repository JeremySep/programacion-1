#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

class Dlgcredits():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ui/winwin.ui")
        self.dialogo = builder.get_object("Dlg_credits")
        self.dialogo.set_title("Creditos")
        cerrar = builder.get_object("btn_close")
        cerrar.connect("clicked", self.on_btn_cerrar_clicked)

    def on_btn_cerrar_clicked(self, btn=None):
            self.dialogo.destroy()